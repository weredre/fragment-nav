package com.rave.colorapp.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.rave.colorapp.R
import com.rave.colorapp.databinding.FragmentExampleBinding

class ExampleFragment:Fragment() {
    private var _binding: FragmentExampleBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?)=
        FragmentExampleBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        binding.cvBlue.setOnClickListener{
//            findNavController().navigate(R.id.action_exampleFragment_to_blueFragment)
//        }
        binding.cvRed.setOnClickListener {
            findNavController().navigate(R.id.action_exampleFragment_to_redFragment)
        }
//        binding.cvGreen.setOnClickListener {
//            findNavController().navigate(R.id.action_exampleFragment_to_greenFragment)
//        }
//        binding.cvGray.setOnClickListener {
//            findNavController().navigate(R.id.action_exampleFragment_to_grayFragment)
//        }
//        binding.cvPurple.setOnClickListener {
//            findNavController().navigate(R.id.action_exampleFragment_to_purpleFragment)
//        }
        binding.cvYellow.setOnClickListener {
            findNavController().navigate(R.id.action_exampleFragment_to_yellowFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

}
