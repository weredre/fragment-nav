package com.rave.colorapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.rave.colorapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    // TODO: Define the lazy delegate 
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    // TODO: What does the override keyword do
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}

        // TODO: What category of kotlin function does with() belong too
//        with(binding) {
//            cvBlue.setOnClickListener { "Blue".toast() }
//            cvGray.setOnClickListener { "Gray".toast() }
//            cvGreen.setOnClickListener { "Green".toast() }
//            cvPurple.setOnClickListener { "Purple".toast() }
//            cvRed.setOnClickListener { "Red".toast() }
//            cvYellow.setOnClickListener { "Yellow".toast() }
//        }
//    }

    // TODO: What kind of function is this?
//    private fun String.toast() {
//        Toast.makeText(this@MainActivity, this, Toast.LENGTH_SHORT).show()
//    }
//}